<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\User;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Projects', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'cost',
            [
                'label' => "Date begin",
                'attribute' => 'datebegin',
                'value' => 'datebegin',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'datebegin',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d'
                        ],
                    ],
                ]),
            ],
            [
                'label' => "Date begin",
                'attribute' => 'dateend',
                'value' => 'dateend',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'dateend',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d'
                        ],
                    ],
                ]),
            ],
            [
                'attribute'=>'user_id',
                'label'=>'User',
                'filter' => Html::activeDropDownList($searchModel, 'user_id', ArrayHelper::map(User::find()->all(),'id','username'),['class'=>'form-control','prompt' => 'Select user']),
                'value'=>"user.username"
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>' {update} {delete}',],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
