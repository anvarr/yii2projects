<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Projects;

/**
 * ProjectsSearch represents the model behind the search form of `app\models\Projects`.
 */
class ProjectsSearch extends Projects
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cost', 'user_id'], 'integer'],
            [['title', 'datebegin', 'dateend'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projects::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cost' => $this->cost,
            /*'datebegin' => $this->datebegin,
            'dateend' => $this->dateend,*/
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        if (!is_null($this->datebegin) && 

            strpos($this->datebegin, ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ', $this->datebegin);

            $query->andFilterWhere(['between', 'date(datebegin)', $start_date, $end_date]);

        }
        if (!is_null($this->dateend) && 

            strpos($this->dateend, ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ', $this->dateend);

            $query->andFilterWhere(['between', 'date(dateend)', $start_date, $end_date]);

        }

        return $dataProvider;
    }
}
