<?php

use yii\db\Migration;


class m181201_152626_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'surname' => $this->string(255),
            'thirdname' => $this->string(255),
            'username' => $this->string(255)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'auth_key' => $this->string(255),
            'access_token' => $this->string(255),
            'status' => $this->integer(1)->defaultValue(0)->notNull(),
        ], $tableOptions);

        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'admin',
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'access_token' => Yii::$app->getSecurity()->generateRandomString(),
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
        ]);

        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'cost' => $this->integer(11),
            'datebegin' => $this->date(),
            'dateend' => $this->date(),
            'user_id' => $this->integer(11)->notNull(),
        ], $tableOptions);

         $this->addForeignKey('FK_user_projects', '{{%projects}}', 'user_id', '{{%user}}', 'id', 'restrict', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_user_projects', '{{%projects}}');
        $this->dropTable('user');
        $this->dropTable('projects');
    }
}
